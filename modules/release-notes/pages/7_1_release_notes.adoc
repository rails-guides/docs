:doctype: book

*DO NOT READ THIS FILE ON GITHUB, GUIDES ARE PUBLISHED ON https://guides.rubyonrails.org.*

= Ruby on Rails 7.1 Release Notes

Highlights in Rails 7.1:

'''

== Upgrading to Rails 7.1

If you're upgrading an existing application, it's a great idea to have good test
coverage before going in. You should also first upgrade to Rails 7.0 in case you
haven't and make sure your application still runs as expected before attempting
an update to Rails 7.1. A list of things to watch out for when upgrading is
available in the
link:upgrading_ruby_on_rails.html#upgrading-from-rails-7-0-to-rails-7-1[Upgrading Ruby on Rails]
guide.

== Major Features

== Railties

Please refer to the https://github.com/rails/rails/blob/main/railties/CHANGELOG.md[Changelog] for detailed changes.

=== Removals

=== Deprecations

=== Notable changes

== Action Cable

Please refer to the https://github.com/rails/rails/blob/main/actioncable/CHANGELOG.md[Changelog] for detailed changes.

=== Removals

=== Deprecations

=== Notable changes

== Action Pack

Please refer to the https://github.com/rails/rails/blob/main/actionpack/CHANGELOG.md[Changelog] for detailed changes.

=== Removals

=== Deprecations

=== Notable changes

== Action View

Please refer to the https://github.com/rails/rails/blob/main/actionview/CHANGELOG.md[Changelog] for detailed changes.

=== Removals

=== Deprecations

=== Notable changes

== Action Mailer

Please refer to the https://github.com/rails/rails/blob/main/actionmailer/CHANGELOG.md[Changelog] for detailed changes.

=== Removals

=== Deprecations

=== Notable changes

== Active Record

Please refer to the https://github.com/rails/rails/blob/main/activerecord/CHANGELOG.md[Changelog] for detailed changes.

=== Removals

=== Deprecations

=== Notable changes

== Active Storage

Please refer to the https://github.com/rails/rails/blob/main/activestorage/CHANGELOG.md[Changelog] for detailed changes.

=== Removals

=== Deprecations

=== Notable changes

== Active Model

Please refer to the https://github.com/rails/rails/blob/main/activemodel/CHANGELOG.md[Changelog] for detailed changes.

=== Removals

=== Deprecations

=== Notable changes

== Active Support

Please refer to the https://github.com/rails/rails/blob/main/activesupport/CHANGELOG.md[Changelog] for detailed changes.

=== Removals

=== Deprecations

=== Notable changes

== Active Job

Please refer to the https://github.com/rails/rails/blob/main/activejob/CHANGELOG.md[Changelog] for detailed changes.

=== Removals

=== Deprecations

=== Notable changes

== Action Text

Please refer to the https://github.com/rails/rails/blob/main/actiontext/CHANGELOG.md[Changelog] for detailed changes.

=== Removals

=== Deprecations

=== Notable changes

== Action Mailbox

Please refer to the https://github.com/rails/rails/blob/main/actionmailbox/CHANGELOG.md[Changelog] for detailed changes.

=== Removals

=== Deprecations

=== Notable changes

== Ruby on Rails Guides

Please refer to the https://github.com/rails/rails/blob/main/guides/CHANGELOG.md[Changelog] for detailed changes.

=== Notable changes

== Credits

See the
https://contributors.rubyonrails.org/[full list of contributors to Rails]
for the many people who spent many hours making Rails, the stable and robust
framework it is. Kudos to all of them.
